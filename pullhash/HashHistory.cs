﻿using System.Collections.ObjectModel;

namespace PullHash
{
    public class HistoryField
    {
        public HistoryField()
        {
            this.HashResults = new ObservableCollection<HashResults>();
        }

        public string FileName { get; set; }
        public string FileSize { get; set; }

        public ObservableCollection<HashResults> HashResults { get; set; }
    }

    public class HashResults
    {
        public string MD5Hash { get; set; }
        public string SHA1Hash { get; set; }
        public string SHA256Hash { get; set; }
    }
}
